﻿using CasinoGenerators.Models;
using CasinoGenerators.Models.Ticket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace CasinoGeneratorsLib
{
    public class TicketGenerator : IInnerGenerator
    {
        private double[] Coefficients;

        private int CellsCount;
        private int TriesCount;
        private double EmptyProbability;
        private double ZeroProbability;

        private List<CellDTO> TurnSequence;
        private int CurrentStep;
        private Random r;

        public TicketGenerator()
        {
            Coefficients = Enumerable.Range(1, 100).Select(i => 1.0d * i / 4).ToArray();
            CellsCount = 25;
            EmptyProbability = 0.3d;
            ZeroProbability = 0.65f;
            TriesCount = 5;

            r = new Random();

            ResetState();
        }

        public void SetConfig(string data)
        {
            var config = JsonSerializer.Deserialize<ConfigTicketDTO>(data);

            if (config.CellsCount > 0)
            {
                CellsCount = config.CellsCount;
            }

            if (config.TriesCount > 0)
            {
                TriesCount = config.TriesCount;
            }

            if (config.ZeroProbability > 0)
            {
                ZeroProbability = config.ZeroProbability;
            }

            EmptyProbability = config.EmptyProbability;
        }

        private void ResetState()
        {
            TurnSequence = new List<CellDTO>();
            CurrentStep = 0;
        }

        public string GenerateSequence(string data, float winRate = 1, float maxMultiplier = 1000)
        {
            if (maxMultiplier <= 0 || winRate <= 0)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = "Parameters winRate and maxMultiplier must be positive" });
            }

            var startData = JsonSerializer.Deserialize<StartGameTicketDTO>(data);
            if (startData == null)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = $"Error while deserializing {nameof(StartGameTicketDTO)}" });
            }

            ResetState();

            var minesCount = (int)Math.Round(ZeroProbability * CellsCount);
            var remainingCells = CellsCount;
            var variance = 1;

            var currentCoefficient = 0d;
            var coef = 1d;

            while (remainingCells > minesCount && CellsCount - remainingCells < TriesCount)
            {
                var bombProb = 1.0d * minesCount / remainingCells;
                if (r.NextDouble() < bombProb)
                {
                    break;
                }

                coef += 1.0d * remainingCells / (remainingCells - minesCount);

                remainingCells--;
            }

            coef *= winRate;
            coef -= 1;
            if (coef != 0)
            {
                coef += (r.NextDouble() * 2 - 1) * variance;
            }
            coef = Math.Max(coef, 0);
            //Console.Write($"C = {coef:0.00} | ");

            // select available coefs
            var coefsCount = new int[Coefficients.Length];
            var coefToSum = Math.Round(coef, 2);
            var currentError = 100d;
            var acceptableError = 0.1d;
            var cntCoefs = 0;
            var isSuccessful = false;
            while (coef > 0 && !isSuccessful && cntCoefs < 10)
            {
                cntCoefs++;
                var minErrTotal = 100d;
                var minErrTotalInd = 0;
                isSuccessful = false;

                var randIndices = Enumerable.Range(0, Coefficients.Length).OrderBy(i => r.Next());

                foreach (var ind in randIndices)
                {
                    currentError = 100d;
                    var newCoef = coefToSum / Coefficients[ind];
                    if (Math.Abs(1 - newCoef) < acceptableError)
                    {
                        coefsCount[ind]++;
                        currentError = Math.Abs(1 - newCoef);
                        isSuccessful = true;
                        break;
                    }

                    var minErrInd = 0;
                    for (int j = Coefficients.Length - 1; j >= 0; j--)
                    {
                        if (Math.Abs(Coefficients[j] - newCoef) < currentError)
                        {
                            currentError = Math.Abs(Coefficients[j] - newCoef);
                            minErrInd = j;

                            if (currentError < minErrTotal)
                            {
                                minErrTotal = currentError;
                                minErrTotalInd = j;
                            }
                        }
                    }

                    if (Math.Abs(1 - newCoef / Coefficients[minErrInd]) < acceptableError)
                    {
                        newCoef /= Coefficients[minErrInd];
                        coefsCount[minErrInd]++;
                        coefsCount[ind]++;
                        currentError = Math.Abs(1 - newCoef);
                        isSuccessful = true;
                        break;
                    }
                }

                if (!isSuccessful)
                {
                    coefToSum /= Coefficients[minErrTotalInd];
                    coefsCount[minErrTotalInd]++;
                }
            }
            

            var steps = CellsCount - remainingCells;
            var freeCells = TriesCount - steps;

            var cnt = 0;
            var selectedSum = 1d;
            var accumulatedCoef = 1d;

            var coefsCountErr = coefsCount.Sum() - steps;

            if (coefsCountErr > 0 && freeCells > 0)
            {
                var transferCnt = Math.Min(freeCells, coefsCountErr);
                freeCells -= transferCnt;
                steps += transferCnt;
                coefsCountErr -= transferCnt;
            }

            while (coefsCountErr > 0)
            {
                for (int i = 0; i < coefsCount.Length; i++)
                {
                    if (coefsCount[i] > 0)
                    {
                        coefsCount[i]--;
                        coefsCountErr--;
                        break;
                    }
                }
            }

            while (cnt < steps)
            {
                if (freeCells > 0 && r.NextDouble() < EmptyProbability)
                {
                    TurnSequence.Add(new CellDTO { CellState = TicketsCellState.Empty, Multiplier = 1 });
                    freeCells--;
                    continue;
                }

                accumulatedCoef += 1.0d * (CellsCount - cnt) / (CellsCount - cnt - minesCount);
                var curMaxCoef = accumulatedCoef * winRate * 2 - selectedSum;
                if (cnt == 0)
                {
                    curMaxCoef -= 1;
                }

                var randIndices = Enumerable.Range(0, Coefficients.Length).OrderBy(i => r.Next());
                var selectedCoef = 0d;

                foreach (var ind in randIndices)
                {
                    if (coefsCount[ind] > 0 && Coefficients[ind] <= curMaxCoef)
                    {
                        selectedCoef = Coefficients[ind];
                        coefsCount[ind]--;
                        break;
                    }
                }

                selectedSum *= selectedCoef;

                if (selectedSum > maxMultiplier)
                {
                    break;
                }

                if (selectedCoef == 0)
                {
                    TurnSequence.Add(new CellDTO { CellState = TicketsCellState.Empty, Multiplier = 1 });
                }
                else
                {
                    TurnSequence.Add(new CellDTO { CellState = TicketsCellState.Multiplier, Multiplier = (float)selectedCoef });
                }

                cnt++;
            }

            TurnSequence.Add(new CellDTO { CellState = TicketsCellState.Mine });

            return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = true });
        }

        public string GetNextStepResult()
        {
            GetClickResultDTOResponse res;
            if (CurrentStep < TurnSequence.Count)
            {
                var cell = TurnSequence[CurrentStep++];
                res = new GetClickResultDTOResponse { Type = cell.CellState, Value = cell.Multiplier };
            }
            else
            {
                res = new GetClickResultDTOResponse { Type = TicketsCellState.Mine };
            }

            return JsonSerializer.Serialize(res);
        }

        public string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs)
        {
            var response = new GetGameSettingsDTOResponse { BidStep = betStep, MinBidStep = minBetValue, IsActiveSession = false };

            if (CurrentStep > 0 && CurrentStep < TurnSequence.Count)
            {
                response.IsActiveSession = true;
                response.History = TurnSequence.Take(CurrentStep).ToArray();
            }

            return JsonSerializer.Serialize(response);
        }
    }
}
