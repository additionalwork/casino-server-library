﻿using System;

namespace CasinoGenerators.Models.Ticket
{
    [Serializable]
    public struct GetClickResultDTOResponse : IDataResponse
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public TicketsCellState Type { get; set; }
        public float Value { get; set; }
    }
}
