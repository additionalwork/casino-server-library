﻿namespace CasinoGenerators.Models.Ticket
{
    public class ConfigTicketDTO
    {
        public int CellsCount { get; set; }
        public double EmptyProbability { get; set; }
        public double ZeroProbability { get; set; }
        public int TriesCount { get; set; }
    }
}
