﻿using System;

namespace CasinoGenerators.Models.Ticket
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep { get; set; }
        public long BidStep { get; set; }
        public bool IsActiveSession { get; set; }
        public CellDTO[] History { get; set; }
    }
}
