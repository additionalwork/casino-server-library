﻿namespace CasinoGenerators.Models.Ticket
{
    public enum TicketsCellState
    {
        Unknown = 0,
        Empty = 1,
        Mine = 2,
        Multiplier = 3,
        Bonus = 4,
    }
}
