﻿using System;

namespace CasinoGenerators.Models.Ticket
{
    [Serializable]
    public struct CellDTO
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public TicketsCellState CellState { get; set; }
        public float Multiplier { get; set; }
    }
}
