﻿using CasinoGenerators.Models;
using CasinoGenerators.Models.Wheel;
using System;
using System.Linq;
using System.Text.Json;

namespace CasinoGeneratorsLib
{
    public class WheelGenerator : IInnerGenerator
    {
        private double[] Coefficients;
        private double[] CoefficientsProbabilities;

        private Random r;

        public WheelGenerator()
        {
            Coefficients = new double[] { 0, 0.1d, 0.5d, 0.75d, 1.25d, 1.5d, 1.75d, 2, 3, 5 };
            CoefficientsProbabilities = new double[Coefficients.Length];

            r = new Random();
        }

        public void SetConfig(string data)
        {
            var config = JsonSerializer.Deserialize<ConfigWheelDTO>(data);
            if (config.Coefficients.Length > 0)
            {
                Coefficients = config.Coefficients;
            }
        }

        public string GenerateSequence(string data, float winRate = 1, float maxMultiplier = 1000)
        {
            if (maxMultiplier <= 0 || winRate <= 0)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = "Parameters winRate and maxMultiplier must be positive" });
            }

            var startData = JsonSerializer.Deserialize<StartGameWheelDTO>(data);
            if (startData == null)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = $"Error while deserializing {nameof(StartGameWheelDTO)}" });
            }

            var coefficientsSum = Coefficients.Sum();

            // finding probabilities using Lagrange multipliers (see: https://math.stackexchange.com/questions/4687571/wheel-of-fortune-design)
            var lang2 = (winRate * Coefficients.Length - coefficientsSum) / (Coefficients.Length * Coefficients.Sum(x => x * x) - coefficientsSum * coefficientsSum);
            var lang1 = (-lang2 * coefficientsSum + 1) / Coefficients.Length;

            var coefficientsProbs = new double[Coefficients.Length];
            for (var i = 0; i < Coefficients.Length; i++)
            {
                coefficientsProbs[i] = Coefficients[i] * lang2 / 2 + lang1 / 2;
            }

            var newCoefficients = Coefficients;
            CoefficientsProbabilities = new double[coefficientsProbs.Length];
            for (var i = 0; i < coefficientsProbs.Length; i++)
            {
                CoefficientsProbabilities[i] = coefficientsProbs[i] / coefficientsProbs.Sum();
            }

            if (CoefficientsProbabilities[CoefficientsProbabilities.Length - 1] < 0)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, 
                   ErrorMessage = "Error while calculating coefficient probabilities. Try to use more evenly distributed coefiicients or to increase winrate" });
            }

            return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = true });
        }

        public string GetNextStepResult()
        {
            var nextVal = r.NextDouble();
            var curInd = 0;
            var curSum = CoefficientsProbabilities[curInd];
            while (nextVal > curSum && curInd < CoefficientsProbabilities.Length - 1)
            {
                curInd++;
                curSum += CoefficientsProbabilities[curInd];
            }

            if (curInd >= CoefficientsProbabilities.Length)
            {
                curInd = CoefficientsProbabilities.Length - 1;
            }

            return JsonSerializer.Serialize(new GetClickResultDTOResponse { Value = (float)Coefficients[curInd] });
        }

        public string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs)
        {
            var response = new GetGameSettingsDTOResponse { BidStep = betStep, MinBidStep = minBetValue };

            return JsonSerializer.Serialize(response);
        }
    }
}
