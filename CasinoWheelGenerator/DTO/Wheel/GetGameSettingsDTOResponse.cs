﻿using System;

namespace CasinoGenerators.Models.Wheel
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep { get; set; }
        public long BidStep { get; set; }
    }
}
