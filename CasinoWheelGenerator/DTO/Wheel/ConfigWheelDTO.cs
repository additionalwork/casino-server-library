﻿using System.Collections.Generic;

namespace CasinoGenerators.Models.Wheel
{
    public class ConfigWheelDTO
    {
        public double[] Coefficients { get; set; }
    }
}
