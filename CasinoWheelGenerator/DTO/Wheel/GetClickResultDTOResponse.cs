﻿using System;

namespace CasinoGenerators.Models.Wheel
{
    [Serializable]
    public struct GetClickResultDTOResponse : IDataResponse
    {
        public float Value { get; set; }
    }
}
