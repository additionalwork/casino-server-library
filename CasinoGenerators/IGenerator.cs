﻿namespace CasinoGeneratorsLib
{
    public interface IGenerator
    {
        string GenerateSequence(string data, float winRate, float maxMultiplier);
        string GetNextStepResult();

        string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs);
        string GetBalanceResponse(long balance);
        string GetEndGameResponse(bool isSuccessful);
        string SetConfig(string data);
    }
}
