﻿namespace CasinoGeneratorsLib
{
    public interface IInnerGenerator
    {
        string GenerateSequence(string data, float winRate, float maxMultiplier);
        string GetNextStepResult();

        string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs);
        void SetConfig(string data);
    }
}
