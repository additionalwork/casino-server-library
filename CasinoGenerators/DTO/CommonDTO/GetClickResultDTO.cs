﻿using System;

namespace CasinoGenerators.Models
{
    [Serializable]
    public class GetClickResultDTO : IDataRequest
    {
        public long UnixTimestamp { get; set; }
    }
}
