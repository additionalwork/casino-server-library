﻿using System;

namespace CasinoGenerators.Models
{
    [Serializable]
    public struct EndGameDTOResponse : IDataResponse
    {
        public bool IsSuccessful { get; set; }
    }
}
