﻿using System;
namespace CasinoGenerators.Models
{
    [Serializable]
    public class StartGameDTO : IDataRequest
    {
        public long BetValue { get; set; }
    }
}
