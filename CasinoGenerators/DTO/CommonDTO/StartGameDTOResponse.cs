﻿using System;

namespace CasinoGenerators.Models
{
    [Serializable]
    public struct StartGameDTOResponse : IDataResponse
    {
        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
    }
}
