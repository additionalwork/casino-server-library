﻿using System;

namespace CasinoGenerators.Models
{
    [Serializable]
    public struct GetBalanceDTOResponse : IDataResponse
    {
        public long Balance { get; set; }
    }
}
