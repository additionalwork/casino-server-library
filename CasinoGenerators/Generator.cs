﻿using CasinoGenerators.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;

namespace CasinoGeneratorsLib
{
    public class Generator : IGenerator
    {
        public string Type;
        public string Ex;

        private IInnerGenerator innerGenerator;
        public Generator(string gameId)
        {
            Type = gameId;
            innerGenerator = GetGenerator(gameId);
        }

        private IInnerGenerator GetGenerator(string gameId)
        {
            IInnerGenerator generator = null;
            try
            {
                /*var fs = File.ReadAllText("gameIdGeneratorMap.json");
                var dict = JsonSerializer.Deserialize<Dictionary<string, string>>(fs);
                var dll = Assembly.Load(dict[gameId + "FileName"]);
                var type = dll.GetType(dict[gameId + "NamespaceClassName"]);*/

                // fileName
                var dll = Assembly.Load($"Casino{gameId}Generator");
                // namespace.className
                var type = dll.GetType($"CasinoGeneratorsLib.{gameId}Generator");
                var obj = Activator.CreateInstance(type);
                generator = obj as IInnerGenerator;
            }
            catch (Exception ex)
            {
                //
                Ex = ex.Message;
            }

            return generator;
        }

        public string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs)
        {
            if (innerGenerator == null)
            {
                return "";
            }

            return innerGenerator.GetGameSettingsResponse(minBetValue, betStep, minBombs, maxBombs);
        }

        public string GenerateSequence(string data, float winRate, float maxMultiplier)
        {
            if (innerGenerator == null)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = $"Error constructing generator: {Ex}" });
            }

            return innerGenerator.GenerateSequence(data, winRate, maxMultiplier);
        }
        public string GetNextStepResult()
        {
            if (innerGenerator == null)
            {
                return "";
            }

            return innerGenerator.GetNextStepResult();
        }

        public string GetBalanceResponse(long balance)
        {
            return JsonSerializer.Serialize(new GetBalanceDTOResponse { Balance = balance });
        }

        public string GetEndGameResponse(bool isSuccessful)
        {
            return JsonSerializer.Serialize(new EndGameDTOResponse { IsSuccessful = isSuccessful });
        }

        public string SetConfig(string data)
        {
            if (innerGenerator == null)
            {
                return JsonSerializer.Serialize(new EndGameDTOResponse { IsSuccessful = false });
            }

            innerGenerator.SetConfig(data);

            return JsonSerializer.Serialize(new EndGameDTOResponse { IsSuccessful = true });
        }
    }
}
