﻿using CasinoGenerators.Models;
using CasinoGenerators.Models.Sapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace CasinoGeneratorsLib
{
    public class SapperGenerator : IInnerGenerator
    {
        private double[] Coefficients;
        private double[] CoefficientProbabilities;

        private int CellsCount;
        private double EmptyProbability;

        private List<CellDTO> TurnSequence;
        private int CurrentStep;
        private Random r;

        public SapperGenerator()
        {
            Coefficients = new double[10] { 0.1d, 0.25d, 0.5d, 0.75d, 1, 2, 2.5d, 3, 5, 7.5d };
            CellsCount = 25;
            EmptyProbability = 0.3d;

            r = new Random();

            ResetState();
        }

        private void ResetState()
        {
            CoefficientProbabilities = new double[0];
            TurnSequence = new List<CellDTO>();
            CurrentStep = 0;
        }

        public void SetConfig(string data)
        {
            var config = JsonSerializer.Deserialize<ConfigSapperDTO>(data);
            if (config.Coefficients.Length > 0)
            {
                Coefficients = config.Coefficients;
            }

            if (config.CellsCount > 0)
            {
                CellsCount = config.CellsCount;
            }

            EmptyProbability = config.EmptyProbability;
        }

        public string GenerateSequence(string data, float winRate = 1, float maxMultiplier = 1000)
        {
            if (maxMultiplier <= 0 || winRate <= 0)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = "Parameters winRate and maxMultiplier must be positive" });
            }

            var startData = JsonSerializer.Deserialize<StartGameSapperDTO>(data);
            if (startData == null)
            {
                return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = false, ErrorMessage = $"Error while deserializing {nameof(StartGameSapperDTO)}" });
            }

            ResetState();

            var minesCount = startData.MinesCount;
            var remainingCells = CellsCount;
            var variance = 1;

            var currentCoefficient = 0d;
            var coef = 1d;

            while (remainingCells > minesCount)
            {
                var bombProb = 1.0d * minesCount / remainingCells;
                if (r.NextDouble() < bombProb)
                {
                    break;
                }

                coef += 1.0d * remainingCells / (remainingCells - minesCount);

                remainingCells--;
            }

            coef *= winRate;
            coef -= 1;
            if (coef != 0)
            {
                coef += (r.NextDouble() * 2 - 1) * variance;
            }
            coef = Math.Max(coef, 0);
            //Console.Write($"C = {coef:0.00} | ");

            // select available coefs
            var coefsCount = new int[Coefficients.Length];
            var coefToSum = Math.Round(coef * 100);
            for (int i = Coefficients.Length - 1; i >= 0; i--)
            {
                coefsCount[i] = (int)Math.Truncate(coefToSum / (Coefficients[i] * 100));
                coefToSum -= coefsCount[i] * Coefficients[i] * 100;
            }

            var steps = CellsCount - remainingCells;
            var freeCells = remainingCells - minesCount;

            var cnt = 0;
            var selectedSum = 0d;
            var accumulatedCoef = 1d;

            var coefsCountErr = coefsCount.Sum() - steps;

            if (coefsCountErr > 0 && freeCells > 0)
            {
                var transferCnt = Math.Min(freeCells, coefsCountErr);
                freeCells -= transferCnt;
                steps += transferCnt;
                coefsCountErr -= transferCnt;
            }

            while (coefsCountErr > 0)
            {
                for (int i = 0; i < coefsCount.Length; i++)
                {
                    if (coefsCount[i] > 0)
                    {
                        coefsCount[i]--;
                        coefsCountErr--;
                        break;
                    }
                }
            }

            while (cnt < steps)
            {
                if (freeCells > 0 && r.NextDouble() < EmptyProbability)
                {
                    TurnSequence.Add(new CellDTO { CellState = SapperCellState.Empty });
                    freeCells--;
                    continue;
                }

                accumulatedCoef *= 1.0d * (CellsCount - cnt) / (CellsCount - cnt - minesCount);
                var curMaxCoef = accumulatedCoef * winRate - selectedSum;
                if (cnt == 0)
                {
                    curMaxCoef -= 1;
                }

                var randIndices = Enumerable.Range(0, Coefficients.Length).OrderBy(i => r.Next());
                var selectedCoef = 0d;

                foreach (var ind in randIndices)
                {
                    if (coefsCount[ind] > 0 && Coefficients[ind] <= curMaxCoef)
                    {
                        selectedCoef = Coefficients[ind];
                        coefsCount[ind]--;
                        break;
                    }
                }

                selectedSum += selectedCoef;

                if (selectedSum > maxMultiplier)
                {
                    break;
                }

                if (selectedCoef == 0)
                {
                    TurnSequence.Add(new CellDTO { CellState = SapperCellState.Empty });
                }
                else
                {
                    TurnSequence.Add(new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = (float)selectedCoef });
                }

                cnt++;
            }

            TurnSequence.Add(new CellDTO { CellState = SapperCellState.Mine });

            return JsonSerializer.Serialize(new StartGameDTOResponse { IsSuccessful = true });
        }

        public string GetNextStepResult()
        {
            GetClickResultDTOResponse res;
            if (CurrentStep < TurnSequence.Count)
            {
                var cell = TurnSequence[CurrentStep++];
                res = new GetClickResultDTOResponse { Type = cell.CellState, Value = cell.Multiplier };
            }
            else
            {
                res = new GetClickResultDTOResponse { Type = SapperCellState.Mine };
            }

            return JsonSerializer.Serialize(res);
        }

        public string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs)
        {
            var response = new GetGameSettingsDTOResponse { BidStep = betStep, MinBidStep = minBetValue, MinBombs = minBombs, MaxBombs = maxBombs, IsActiveSession = false };

            if (CurrentStep > 0 && CurrentStep < TurnSequence.Count)
            {
                response.IsActiveSession = true;
                response.History = TurnSequence.Take(CurrentStep).ToArray();
            }

            return JsonSerializer.Serialize(response);
        }
    }
}
