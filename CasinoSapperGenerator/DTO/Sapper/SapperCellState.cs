﻿namespace CasinoGenerators.Models.Sapper
{
    public enum SapperCellState
    {
        Unknown = 0,
        Empty = 1,
        Mine = 2,
        Multiplier = 3,
    }
}
