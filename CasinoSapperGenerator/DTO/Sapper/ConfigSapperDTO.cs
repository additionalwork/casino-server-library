﻿namespace CasinoGenerators.Models.Sapper
{
    public class ConfigSapperDTO
    {
        public double[] Coefficients { get; set; }
        public int CellsCount { get; set; }
        public double EmptyProbability { get; set; }
    }
}
