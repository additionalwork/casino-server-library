﻿using System;

namespace CasinoGenerators.Models.Sapper
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep { get; set; }
        public long BidStep { get; set; }
        public int MinBombs { get; set; }
        public int MaxBombs { get; set; }
        public bool IsActiveSession { get; set; }
        public CellDTO[] History { get; set; }
    }
}
