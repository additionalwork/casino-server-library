﻿using System;

namespace CasinoGenerators.Models.Sapper
{
    [Serializable]
    public struct CellDTO
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public SapperCellState CellState { get; set; }
        public float Multiplier { get; set; }
    }
}
