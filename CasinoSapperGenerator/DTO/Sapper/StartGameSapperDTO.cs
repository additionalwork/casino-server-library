﻿namespace CasinoGenerators.Models.Sapper
{
    public class StartGameSapperDTO : StartGameDTO
    {
        public long BetValue { get; set; }
        public int MinesCount { get; set; }
    }
}
