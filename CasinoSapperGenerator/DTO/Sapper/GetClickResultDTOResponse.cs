﻿using System;

namespace CasinoGenerators.Models.Sapper
{
    [Serializable]
    public struct GetClickResultDTOResponse : IDataResponse
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public SapperCellState Type { get; set; }
        public float Value { get; set; }
    }
}
