﻿using CasinoGenerators;
using CasinoGeneratorsLib;

var generator = new Generator("Wheel");
generator.SetConfig("{\"Coefficients\" : [0, 1, 2] }");
var test = new SapperGeneratorTest(generator);

//test.RunGame(10, 1, 14);

for (int i = 1; i <= 1; i++)
{
    Console.WriteLine($"----------{i} Bombs---------");
    test.Run(triesCount: 100000, bombsCount: i, winrate: 1d, maxCoefficient: 1000, false);
    Console.WriteLine($"----------------------------");
}
