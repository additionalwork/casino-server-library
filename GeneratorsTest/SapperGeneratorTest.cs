﻿using CasinoGenerators.Models.Sapper;
using CasinoGeneratorsLib;
using System.Text.Json;

namespace CasinoGenerators
{
    public class SapperGeneratorTest
    {
        private IGenerator generator;
        private Random r;

        public SapperGeneratorTest(IGenerator _generator)
        {
            generator = _generator;
            r = new Random();
        }

        public void Run(int triesCount, int bombsCount, double winrate, double maxCoefficient, bool toDump)
        {
            Console.WriteLine("Sapper generator tests:");
            Console.WriteLine("---------------------");
            var meanMaxCoef = 0d;
            var meanMinCoef = 0d;
            var meanRandCoef = 0d;
            var meanTrueRandCoef = 0d;
            var maxCoef = 0d;

            var bet = 100;
            var balance = 0d;
            for (int i = 0; i < triesCount; i++)
            {
                if (toDump)
                {
                    Console.Write($"{i}) ");
                }

                var data = JsonSerializer.Serialize(new StartGameSapperDTO { BetValue = 100, MinesCount = bombsCount });
                generator.GenerateSequence(data, (float)winrate, (float)maxCoefficient);

                var res = 1d;
                var minCoef = 0d;
                var randCoef = 0d;
                var setRand = false;
                var setMin = false;
                var curType = JsonSerializer.Deserialize<GetClickResultDTOResponse>(generator.GetNextStepResult());
                var coefs = new List<float>();
                while (curType.Type != SapperCellState.Mine)
                {
                    if (toDump)
                    {
                        Console.Write($"|{curType.Value}| ");
                    }
                    if ((generator as Generator).Type == "Ticket")
                    {
                        res *= curType.Value;
                    }
                    if ((generator as Generator).Type == "Wheel")
                    {
                        setMin = true;
                        minCoef = res;
                        setRand = true;
                        randCoef = res;
                        res = curType.Value;
                        break;
                    }
                    else
                    {
                        res += curType.Value;
                    }

                    coefs.Add(curType.Value);

                    if (!setMin)
                    {
                        setMin = true;
                        minCoef = res;
                    }

                    if (!setRand && r.Next(2) == 0)
                    {
                        setRand = true;
                        randCoef = res;
                    }

                    curType = JsonSerializer.Deserialize<GetClickResultDTOResponse>(generator.GetNextStepResult());
                }

                coefs.Add(-1);
                var trueRandInd = r.Next(coefs.Count);
                if (trueRandInd == coefs.Count - 1)
                {
                    meanTrueRandCoef += -1;
                }
                else
                {
                    meanTrueRandCoef += 1;
                    for (int j = 0; j < trueRandInd; j++)
                    {
                        meanTrueRandCoef += coefs[j];
                    }
                }

                if (!setMin)
                {
                    minCoef = -1;
                    res = -1;
                }

                balance -= bet;
                if (res > 0)
                {
                    balance += res * bet;
                }

                if (res > maxCoef)
                {
                    maxCoef = res;
                }
                
                meanMaxCoef += res;
                meanMinCoef += minCoef;

                if (!setRand)
                {
                    randCoef = -1;
                }
                meanRandCoef += randCoef;

                if (toDump)
                {
                    Console.WriteLine($"Resulted coefficient = {res: 0.000}");
                }
            }

            Console.WriteLine($"Mean max_coefficient_sum = {meanMaxCoef / triesCount : 0.000}");
            Console.WriteLine($"max_coefficient = {maxCoef : 0.000}");
            Console.WriteLine($"Mean min_coefficient_endgame = {meanMinCoef / triesCount : 0.000}");
            Console.WriteLine($"Mean rand_endgame= {meanRandCoef / triesCount : 0.000}");
            Console.WriteLine($"Mean true_rand_endgame= {meanTrueRandCoef / triesCount : 0.000}");
            Console.WriteLine($"Balance mean= {balance / triesCount: 0.000}");
            Console.WriteLine("---------------------");
        }

        public void RunGame(int minesCount, double winrate, double maxCoefficient)
        {
            var state = SapperCellState.Mine;
            var balance = 1000d;
            var bet = 100;
            var mult = 1d;
            while (true)
            {
                if (state == SapperCellState.Mine)
                {
                    balance -= bet;
                    mult = 1;
                    var data = JsonSerializer.Serialize(new StartGameSapperDTO { BetValue = 100, MinesCount = minesCount });
                    generator.GenerateSequence(data, (float)winrate, (float)maxCoefficient);
                }

                Console.WriteLine($"BALANCE = {balance}");
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Spacebar)
                {
                    var type = JsonSerializer.Deserialize<GetClickResultDTOResponse>(generator.GetNextStepResult());
                    state = type.Type;
                    if (state == SapperCellState.Mine)
                    {
                        Console.WriteLine($"BOMB!");
                    }
                    else
                    {
                        mult += type.Value;
                        Console.WriteLine($"MULT = {mult}");
                    }
                }

                if (key.Key == ConsoleKey.F)
                {
                    Console.WriteLine($"+{bet * mult}");
                    balance += bet * mult;
                    state = SapperCellState.Mine;
                }
            }
        }
    }    
}
